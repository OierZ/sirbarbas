import { Objeto } from "./objeto";
import { Skin } from "./skin";
/**
 *
 *
 * @export
 * @class Equipamiento
 */
export class Equipamiento {
  cabeza: Objeto;
  cuello: Objeto;
  anillo1: Objeto;
  anillo2: Objeto;
  arma1: Objeto;
  arma2: Objeto;
  pecho: Objeto;
  item1: Objeto;
  item2: Objeto;
  item3: Objeto;
  manos: Objeto;
  pies: Objeto;
  diploma: Objeto;
  skin: string;
  inventario: Objeto[];
  skins: Skin[];

  /**
   *Creates an instance of Equipamiento.
   * @memberof Equipamiento
   */
  constructor() {
    this.cabeza = null;
    this.cuello = null;
    this.anillo1 = null;
    this.anillo2 = null;
    this.arma1 = null;
    this.arma2 = null;
    this.pecho = null;
    this.item1 = null;
    this.item2 = null;
    this.item3 = null;
    this.manos = null;
    this.pies = null;
    this.diploma = null;
    this.inventario = [];
    this.skin = "";
    this.skins = [];
  }
}
