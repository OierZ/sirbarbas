export class User {
  userId: string;
  userName: string;
  displayName: string;
  level: number;
  xp: number;
  damage: number;
  health: number;
  armor: number;
  curation: number;
  rank: string;
  skinName: string;
  money: number;
  moneyEarned: number;
  userClass: string;
  subClass: string;
  lore: string;
  loreState: number;
  timeSpent: number;
  roles: string;
  broadcasterType: string;
  userType: string;
  viewCount: number;
  createdAt: string;

  constructor() {
    this.userId = "";
    this.userName = "";
    this.displayName = "";
    this.level = 0;
    this.xp = 0;
    this.damage = 1;
    this.health = 10;
    this.armor = 0;
    this.curation = 0;
    this.rank = "Recluta";
    this.skinName = "barbita";
    this.money = 0;
    this.moneyEarned = 0;
    this.userClass = "";
    this.subClass = "";
    this.lore = "";
    this.loreState = 1;
    this.timeSpent = 0;
    this.roles = "viewer";
    this.broadcasterType = "";
    this.userType = "";
    this.viewCount = 0;
    this.createdAt = "";
  }
}
