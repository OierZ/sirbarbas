/**
 *
 *
 * @export
 * @interface Objeto
 */
export interface Objeto {
  itemId: number;
  active: number;
  damageBonus: number;
  armorBonus: number;
  healthBonus: number;
  curationBonus: number;
  name: string;
  description: string;
  picture: string;
  card: string;
  location: string;
}
