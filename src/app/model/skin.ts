export class Skin {
  skinName: string;
  roles: string;
  createdAt: string;
  userCount: number;
  active: number;
  picture: string;
  belongs: string;
}
