export class Inventario {
  userId: string;
  skins: string;
  headItems: string;
  neckItems: string;
  ringItems: string;
  weaponItems: string;
  handItems: string;
  torsoItems: string;
  feetItems: string;
  regularItems: string;
  specialItems: string;
  skinSlot: string;
  headSlot: number;
  neckSlot: number;
  ringSlot1: number;
  ringSlot2: number;
  weaponSlot1: number;
  weaponSlot2: number;
  handSlot: number;
  torsoSlot: number;
  feetSlot: number;
  itemSlot1: number;
  itemSlot2: number;
  itemSlot3: number;
  specialItemSlot: number;

  constructor() {
    this.userId = "";
    this.skins = "";
    this.headItems = "";
    this.neckItems = "";
    this.ringItems = "";
    this.weaponItems = "";
    this.handItems = "";
    this.torsoItems = "";
    this.feetItems = "";
    this.regularItems = "";
    this.specialItems = "";
    this.skinSlot = "";
    this.headSlot = 0;
    this.neckSlot = 0;
    this.ringSlot1 = 0;
    this.ringSlot2 = 0;
    this.weaponSlot1 = 0;
    this.weaponSlot2 = 0;
    this.handSlot = 0;
    this.torsoSlot = 0;
    this.feetSlot = 0;
    this.itemSlot1 = 0;
    this.itemSlot2 = 0;
    this.itemSlot3 = 0;
    this.specialItemSlot = 0;
  }
}
