import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatDividerModule } from "@angular/material";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSliderModule } from "@angular/material/slider";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCarouselModule } from "@ngmodule/material-carousel";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CookiesBannerComponent } from "./components/cookies-banner/cookies-banner.component";
import { CanjearCodigoDialog } from "./components/dialog/canjear-codigo-dialog.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HeaderComponent } from "./components/header/header.component";
import { StreamComponent } from "./components/stream/stream.component";
import { BackendComponent } from "./pages/backend/backend.component";
import { CookiesComponent } from "./pages/cookies/cookies.component";
import { LandingComponent } from "./pages/landing/landing.component";
import { LoreComponent } from "./pages/lore/lore.component";
import { PersonajesComponent } from "./pages/lore/personajes/personajes.component";
import { QuienSoyComponent } from "./pages/quien-soy/quien-soy.component";
import { RankingComponent } from "./pages/ranking/ranking.component";
import { LoginComponent } from "./pages/rpg/login/login.component";
import { PersonajeComponent } from "./pages/rpg/personaje/personaje.component";
import { RpgComponent } from "./pages/rpg/rpg.component";
import { TwitchComponent } from "./pages/twitch/twitch.component";
import { SafePipe } from "./pipe/safe.pipe";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingComponent,
    QuienSoyComponent,
    LoreComponent,
    PersonajesComponent,
    TwitchComponent,
    RankingComponent,
    PersonajeComponent,
    HeaderComponent,
    StreamComponent,
    SafePipe,
    RpgComponent,
    CanjearCodigoDialog,
    FooterComponent,
    BackendComponent,
    CookiesComponent,
    CookiesBannerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatCarouselModule.forRoot(),
    HttpClientModule,
    FormsModule,
    MatTabsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CanjearCodigoDialog],
})
export class AppModule {}
