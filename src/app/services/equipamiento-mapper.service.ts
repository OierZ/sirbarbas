import { Injectable } from "@angular/core";
import { Skin } from "../model/skin";
import { Equipamiento } from "./../model/equipamiento";
import { Inventario } from "./../model/inventario";
import { Objeto } from "./../model/objeto";

/**
 *
 *
 * @export
 * @class EquipamientoMapperService
 */

@Injectable({
  providedIn: "root"
})
export class EquipamientoMapperService {
  /**
   *
   * @param {*} equipo
   * @param {Objeto[]} items
   * @returns {Equipamiento}
   * @memberof EquipamientoMapperService
   */
  getEquipamiento(equipo, items: Objeto[], skins: Skin[]): Equipamiento {
    let equipamiento: Equipamiento = new Equipamiento();
    equipamiento.anillo1 = items.find(x => x.itemId == equipo.ringSlot1);
    equipamiento.anillo2 = items.find(x => x.itemId == equipo.ringSlot2);
    equipamiento.arma1 = items.find(x => x.itemId == equipo.weaponSlot1);
    equipamiento.arma2 = items.find(x => x.itemId == equipo.weaponSlot2);
    equipamiento.cabeza = items.find(x => x.itemId == equipo.headSlot);
    equipamiento.cuello = items.find(x => x.itemId == equipo.neckSlot);
    equipamiento.diploma = items.find(x => x.itemId == equipo.specialSlot);
    equipamiento.item1 = items.find(x => x.itemId == equipo.itemSlot1);
    equipamiento.item2 = items.find(x => x.itemId == equipo.itemSlot2);
    equipamiento.item3 = items.find(x => x.itemId == equipo.itemSlot3);
    equipamiento.manos = items.find(x => x.itemId == equipo.handSlot);
    equipamiento.pecho = items.find(x => x.itemId == equipo.torsoSlot);
    equipamiento.pies = items.find(x => x.itemId == equipo.feetSlot);
    equipamiento.skin = equipo.skinSlot;

    let arrayTMP = [];

    arrayTMP = arrayTMP
      .concat(equipo.ringItems.split(" "))
      .concat(equipo.weaponItems.split(" "))
      .concat(equipo.headItems.split(" "))
      .concat(equipo.neckItems.split(" "))
      .concat(equipo.specialItems.split(" "))
      .concat(equipo.regularItems.split(" "))
      .concat(equipo.handItems.split(" "))
      .concat(equipo.torsoItems.split(" "))
      .concat(equipo.feetItems.split(" "));
    arrayTMP.forEach(element => {
      let item = items.find(x => x.itemId == element);
      if (item) equipamiento.inventario.push(item);
    });

    let arraySkins = equipo.skins.split(" ");

    let arraySkinsTMP = [];
    arraySkins.forEach(element => {
      arraySkinsTMP.push(skins.find(x => x.skinName == element));
    });
    equipamiento.skins = arraySkinsTMP;

    return equipamiento;
  }

  /**
   *
   * @param {Equipamiento} equipo
   * @memberof EquipamientoMapperService
   */
  sendEquipamiento(equipo: Equipamiento) {
    let inventario: Inventario = new Inventario();
    inventario.skinSlot = equipo.skin;
    inventario.feetSlot = equipo.pies ? equipo.pies.itemId : 0;
    inventario.handSlot = equipo.manos ? equipo.manos.itemId : 0;
    inventario.headSlot = equipo.cabeza ? equipo.cabeza.itemId : 0;
    inventario.itemSlot1 = equipo.item1 ? equipo.item1.itemId : 0;
    inventario.itemSlot2 = equipo.item2 ? equipo.item2.itemId : 0;
    inventario.itemSlot3 = equipo.item3 ? equipo.item3.itemId : 0;
    inventario.neckSlot = equipo.cuello ? equipo.cuello.itemId : 0;
    inventario.ringSlot1 = equipo.anillo1 ? equipo.anillo1.itemId : 0;
    inventario.ringSlot2 = equipo.anillo2 ? equipo.anillo2.itemId : 0;
    inventario.torsoSlot = equipo.pecho ? equipo.pecho.itemId : 0;
    inventario.weaponSlot1 = equipo.arma1 ? equipo.arma1.itemId : 0;
    inventario.weaponSlot2 = equipo.arma2 ? equipo.arma2.itemId : 0;
    inventario.specialItemSlot = equipo.diploma ? equipo.diploma.itemId : 0;

    let skinsTMP = "";
    equipo.skins.forEach(skin => {
      skinsTMP += skin.skinName + " ";
    });
    inventario.skins = skinsTMP.trim();

    let arrayTMP: Objeto[] = [];

    arrayTMP = equipo.inventario.filter(x => x.location == "feet_slot");
    arrayTMP.forEach(element => {
      inventario.feetItems += " " + element.itemId;
    });
    inventario.feetItems = inventario.feetItems.trim();

    arrayTMP = equipo.inventario.filter(x => x.location == "hand_slot");
    arrayTMP.forEach(element => {
      inventario.handItems += " " + element.itemId;
    });
    inventario.handItems = inventario.handItems.trim();

    arrayTMP = equipo.inventario.filter(x => x.location == "head_slot");
    arrayTMP.forEach(element => {
      inventario.headItems += " " + element.itemId;
    });
    inventario.headItems = inventario.headItems.trim();
    arrayTMP = equipo.inventario.filter(x => {
      if (x.location.includes("item_slot_1")) {
        return x;
      } else if (x.location.includes("item_slot_2")) {
        return x;
      } else if (x.location.includes("item_slot_3")) {
        return x;
      }
    });
    arrayTMP.forEach(element => {
      inventario.regularItems += " " + element.itemId;
    });
    inventario.regularItems = inventario.regularItems.trim();

    arrayTMP = equipo.inventario.filter(x => x.location == "neck_slot");
    arrayTMP.forEach(element => {
      inventario.neckItems += " " + element.itemId;
    });
    inventario.neckItems = inventario.neckItems.trim();

    arrayTMP = equipo.inventario.filter(x => {
      if (x.location.includes("ring_slot_1")) {
        return x;
      } else if (x.location.includes("ring_slot_2")) {
        return x;
      }
    });
    arrayTMP.forEach(element => {
      inventario.regularItems += " " + element.itemId;
    });
    inventario.regularItems = inventario.regularItems.trim();

    arrayTMP = equipo.inventario.filter(x => x.location == "torso_slot");
    arrayTMP.forEach(element => {
      inventario.torsoItems += " " + element.itemId;
    });
    inventario.torsoItems = inventario.torsoItems.trim();

    arrayTMP = equipo.inventario.filter(x => {
      if (x.location.includes("weapon_slot_1")) {
        return x;
      } else if (x.location.includes("weapon_slot_2")) {
        return x;
      }
    });
    arrayTMP.forEach(element => {
      inventario.weaponItems += " " + element.itemId;
    });
    inventario.weaponItems = inventario.weaponItems.trim();

    arrayTMP = equipo.inventario.filter(x => x.location == "special_item_slot");
    arrayTMP.forEach(element => {
      inventario.specialItems += " " + element.itemId;
    });
    inventario.specialItems = inventario.specialItems.trim();

    return inventario;
  }
}
