import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "./../../environments/environment";
import { RpgService } from "./rpg.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private client_id: string = "scqehrhkoxb3xx3s262ftf5xcm4l8c";
  private redirect: string = environment.url + "/rpg";
  private response_type: string = "token";
  constructor(
    private http: HttpClient,
    private router: Router,
    private rpg: RpgService
  ) {}

  loginTwitch() {
    location.href =
      "https://id.twitch.tv/oauth2/authorize?client_id=" +
      this.client_id +
      "&redirect_uri=" +
      this.redirect +
      "&response_type=" +
      this.response_type;
  }

  getUserId() {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.getAccessToken());

    return this.http.get("https://api.twitch.tv/helix/users", {
      headers,
    });
  }

  setAccessToken(token) {
    localStorage.setItem("access_token", token);
  }

  getAccessToken() {
    return localStorage.getItem("access_token");
  }

  comprobarAuth(url) {
    if (this.getAccessToken() == null) {
      if (url != "/rpg/login") this.router.navigate(["rpg", "login"]);
    } else {
      this.getUserId().subscribe(
        (data: any) => {
          this.rpg.twitch_user = data.data[0];
          this.router.navigate(["rpg", "personaje"]);
        },
        (err) => {
          this.router.navigate(["rpg", "login"]);
        }
      );
    }
  }
}
