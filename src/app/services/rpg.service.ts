import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Inventario } from "./../model/inventario";
import { User } from "./../model/user";

@Injectable({
  providedIn: "root",
})
export class RpgService {
  public twitch_user;
  private POST: string = "POST";
  private GET: string = "GET";
  private PUT: string = "PUT";
  private DELETE: string = "DELETE";
  public LORESTATE_VERIFICADO = 1;
  public LORESTATE_PENDIENTE = 0;
  public LORESTATE_BLOQUEADO = -1;

  private BASE_URL: string = "https://api.sirbarbas.com/api/barbota/";
  private API_KEY: string = "c2c6c0a0ad5b4131a6a2abcd62db7eda";

  private URL_USERS: string = "users/";
  private URL_INVENTARIOS: string = "inventories/";
  private URL_ITEMS: string = "items/";
  private URL_SKINS: string = "skins/";

  constructor(private http: HttpClient) {}

  private request(method, path, body?) {
    let url = this.BASE_URL + path;
    let headers = this.getHeaders();

    return this.http.request(method, url, {
      headers: headers,
      body: body,
    });
  }

  private getHeaders(): HttpHeaders {
    let headers = new HttpHeaders({
      "X-API-KEY": this.API_KEY,
    });
    return headers;
  }

  getUser() {
    return this.request(this.GET, this.URL_USERS + this.twitch_user.id);
  }

  getRankingNivel() {
    return this.request(this.GET, this.URL_USERS + "levelRanking");
  }

  getRankingPuntos() {
    return this.request(this.GET, this.URL_USERS + "moneyRanking");
  }

  getRankingPuntosMes() {
    return this.request(this.GET, this.URL_USERS + "moneyEarnedRanking");
  }

  setUser() {
    let body: User = new User();

    body.userId = this.twitch_user.id;
    body.userName = this.twitch_user.login;
    body.displayName = this.twitch_user.display_name;
    return this.request(this.POST, this.URL_USERS, body);
  }

  getInventario() {
    return this.request(this.GET, this.URL_INVENTARIOS + this.twitch_user.id);
  }

  setInventario() {
    let inv: Inventario = new Inventario();
    inv.userId = this.twitch_user.id + "";

    return this.request(this.POST, this.URL_INVENTARIOS, inv);
  }

  updateInventario(inv: Inventario) {
    inv.userId = this.twitch_user.id + "";
    return this.request(this.PUT, this.URL_INVENTARIOS + inv.userId, inv);
  }

  getItems() {
    return this.request(this.GET, this.URL_ITEMS);
  }
  getItem(id) {
    return this.request(this.GET, this.URL_ITEMS + id);
  }

  getSkins() {
    return this.request(this.GET, this.URL_SKINS);
  }
  setSkin(skin) {
    let body = { skinSlot: skin };
    return this.request(
      this.PUT,
      this.URL_INVENTARIOS + this.twitch_user.id,
      body
    );
  }

  setLore(lore: string) {
    let body = {
      lore: lore,
      loreState: this.LORESTATE_PENDIENTE,
    };

    return this.request(this.PUT, this.URL_USERS + this.twitch_user.id, body);
  }
}
