import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "./pages/landing/landing.component";
import { LoreComponent } from "./pages/lore/lore.component";
import { PersonajesComponent } from "./pages/lore/personajes/personajes.component";
import { QuienSoyComponent } from "./pages/quien-soy/quien-soy.component";
import { RankingComponent } from "./pages/ranking/ranking.component";
import { TwitchComponent } from "./pages/twitch/twitch.component";

const routes: Routes = [
  { path: "", component: LandingComponent },
  { path: "quien-soy", component: QuienSoyComponent },
  { path: "lore", component: LoreComponent },
  { path: "personajes", component: PersonajesComponent },
  { path: "twitch", component: TwitchComponent },
  { path: "ranking", component: RankingComponent },
  { path: "**", component: LandingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
