import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { environment } from "src/environments/environment";

@Component({
  selector: "barba-stream",
  templateUrl: "./stream.component.html",
  styleUrls: ["./stream.component.scss"],
})
export class StreamComponent implements OnInit {
  srcStream;
  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.srcStream = this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://player.twitch.tv/?channel=sirbarbas&muted=true&parent=" +
        environment.domain
    );
  }
}
