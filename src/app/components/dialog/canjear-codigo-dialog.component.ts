import { Component } from "@angular/core";
@Component({
  selector: "canjear-codigo-dialog",
  templateUrl: "canjear-codigo-dialog.component.html",
  styleUrls: ["./canjear-codigo-dialog.component.scss"]
})
export class CanjearCodigoDialog {
  clave: string = "";
  llaveAparecer = false;
  llaveClick = false;
  llaveGirar = false;
  cartaMostrar = false;

  keyPress(ev) {
    if (ev.target.value.length == 36) {
      this.llaveAparecer = true;
    } else {
      this.llaveAparecer = false;
    }
  }

  clickLlave() {
    this.llaveClick = true;

    setTimeout(() => {
      this.llaveGirar = true;
      setTimeout(() => {
        this.cartaMostrar = true;
      }, 500);
    }, 3000);
  }
}
