import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "barba-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @ViewChild("menu", null) menu: ElementRef;
  @ViewChild("burger", { static: true }) burger: ElementRef;
  abierto: boolean = false;

  constructor(private router: Router, private renderer: Renderer2) {
    this.renderer.listen("window", "click", (e: Event) => {
      if (
        e.target !== this.menu.nativeElement &&
        e.target !== this.burger.nativeElement
      ) {
        this.abierto = false;
      }
    });
  }

  ngOnInit() {}

  goToLanding() {
    this.router.navigate([""]);
  }

  clickBurger() {
    this.abierto = !this.abierto;
  }
}
