import { Component, OnInit } from "@angular/core";

@Component({
  selector: "barba-cookies-banner",
  templateUrl: "./cookies-banner.component.html",
  styleUrls: ["./cookies-banner.component.scss"],
})
export class CookiesBannerComponent implements OnInit {
  ocultarBanner = false;

  constructor() {}

  ngOnInit() {}

  cerrarBanner() {
    this.ocultarBanner = true;
    setTimeout(() => {
      localStorage.setItem("cookiesHide", "x");
    }, 600);
  }
}
