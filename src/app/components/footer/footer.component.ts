import { Component, OnInit } from "@angular/core";
import { version } from "../../../../package.json";
@Component({
  selector: "barba-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
  redes = [
    { enlace: "https://www.twitch.tv/sirbarbas", icono: "ico_twitch" },
    { enlace: "https://twitter.com/SirBarbas", icono: "ico_twitter" },
    { enlace: "https://www.instagram.com/sirbarbas/", icono: "ico_instagram" },
    {
      enlace:
        "https://www.youtube.com/channel/UC3z7eiCqS2wiS5nK1Pk74dQ?view_as=subscriber",
      icono: "ico_youtube",
    },
    {
      enlace: "https://www.facebook.com/SirBarbasOficial/",
      icono: "ico_facebook",
    },
    {
      enlace: "mailto:info@sirbarbas.com",
      icono: "ico_email",
    },
  ];
  public version: string = version;

  constructor() {}

  ngOnInit() {}

  goToRRSS(item) {
    window.open(item.enlace, "_blank");
  }
}
