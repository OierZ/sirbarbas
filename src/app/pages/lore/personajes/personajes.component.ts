import { Component, OnInit } from "@angular/core"

export interface Slide {
  titulo: String
  texto: String
  imagen: String
  fondo: String
  float: String
}

@Component({
  selector: "app-personajes",
  templateUrl: "./personajes.component.html",
  styleUrls: ["./personajes.component.scss"]
})
export class PersonajesComponent implements OnInit {
  slidesLuz: Slide[] = [
    {
      titulo: "Sir Barbas",
      texto:
        "Caballero de La Orden de la Luz encargado de defender nuestra realidad de las hordas del ejercito de La Oscuridad. \nMuchas son las historias que se han contado de él, pero nadie, sabe su verdadero origen. ¿Te atreves a descubrirlo?",
      imagen: "./../../../assets/img/barbarmor.png",
      fondo: "#28877463",
      float: "d"
    },
    {
      titulo: "Barbitas",
      texto:
        "Creados por el Señor de la Luz cuando Sir Barbas estaba encarcelado en la fortaleza de la Oscuridad. \nLas Barbitas, al ser seres duales, están formados por una mitad oscura y otra mitad luminosa que luchan eternamente, de ahí a que tengan ese gesto de felicidad y locura en su propio rostro. \nHan luchado al lado de Sir Barbas desde el principio de los días, haciendo una promesa de defender cada dimensión de las garras de la Oscuridad. ",
      imagen: "./../../../assets/img/barba.png",
      fondo: "#9b770026",
      float: "d"
    },
    {
      titulo: "Barbitas",
      texto:
        "Lo más antiguos del lugar lo llaman Merk, el guardián. Muchas son las historias que se cuentan de él. Se habla de sus inventos, de sus locuras y no siempre para bien. Muchas son las veces que inventó algo que resulto ser la misma pura oscuridad… \nA su lado, siempre está Kappi, una de sus mejores creaciones, el aliado que todo héroe quisiera tener, capaz de detectar a los ejércitos de la oscuridad en cualquier realidad, en cualquier universo.",
      imagen: "./../../../assets/img/merk.png",
      fondo: "#ff477517",
      float: "d"
    },
    {
      titulo: "Caballeros de sir barbas",
      texto:
        "La Guardia Real de Sir Barbas. Fieles combatientes que han jurado proteger y servir a la Orden de la Luz. \nPara entrar en esta Guardia es necesario mostrar su valentía en innumerables pruebas contra La Orden de la Oscuridad.",
      imagen: "./../../../assets/img/barbitas.png",
      fondo: "#18a6e2a1",
      float: "d"
    }
  ]

  slidesOscuridad: Slide[] = [
    {
      titulo: "badbitas",
      texto:
        "A lo largo de la historia infinidad de Barbitas han sido sucumbidas por poderosos Caballeros de la Oscuridad, transformándose en Badbitas y acabando con todo su lado luminoso. \nSon seres malvados, destinados a terminar con cada una de las Barbitas de cada realidad. Carecen de sentimientos, no se preocupan por los demás, tan solo quieren la eterna oscuridad. ",
      imagen: "./../../../assets/img/badbita.png",
      fondo: "#331710bd",
      float: "i"
    },
    {
      titulo: "sombras oscuras",
      texto:
        "Las Sombras Oscuras son almas de viejos caballeros que han sucumbido a la oscuridad. \nSon almas que van vagando por el mundo consumiendo la energía de los seres más débiles. Cuenta la leyenda que cada madrugada se les oye discutir en soledad, como si una parte de su luz quisiera luchar contra la oscuridad. \nAl ser convertidos por la oscuridad, los caballeros se aislan del mundo y permanecen ocultos durante varios meses. Durante estos meses no se alimentan, no se relacionan, sus miedos y sus inseguridades terminan consumiendo su propio cuerpo y convirtiendo su luz en sombras oscuras. ",
      imagen: "./../../../assets/img/sombras.png",
      fondo: "#e0bc2f6b",
      float: "i"
    }
  ]

  alineacion(o) {
    if (o == "i") {
      return "3"
    } else {
      return "1"
    }
  }

  constructor() {}

  ngOnInit() {}
}
