import { Component, OnInit } from "@angular/core"
import { Title } from "@angular/platform-browser"

@Component({
  selector: "app-lore",
  templateUrl: "./lore.component.html",
  styleUrls: ["./lore.component.scss"]
})
export class LoreComponent implements OnInit {
  videos = [
    { titulo: "lore1", url: "https://www.youtube.com/embed/FBf4zIxajGU" },
    { titulo: "lore2", url: "https://www.youtube.com/embed/G_NVWd3Dfcc" }
  ]

  constructor(private title: Title) {
    title.setTitle("LORE - SirBarbas")
  }

  ngOnInit() {}
}
