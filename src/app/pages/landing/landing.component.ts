import { Title } from "@angular/platform-browser"
import { Router } from "@angular/router"
import { Component, OnInit } from "@angular/core"

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"]
})
export class LandingComponent implements OnInit {
  constructor(private router: Router, private title: Title) {
    title.setTitle("SirBarbas")
  }

  ngOnInit() {}
  ngAfterViewInit() {
    setTimeout(() => {
      this.router.navigate(["/quien-soy"])
    }, 3000)
  }
}
