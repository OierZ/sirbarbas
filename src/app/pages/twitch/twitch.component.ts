import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-twitch",
  templateUrl: "./twitch.component.html",
  styleUrls: ["./twitch.component.scss"],
})
export class TwitchComponent implements OnInit {
  srcChat;
  srcStream;
  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.srcChat = this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://www.twitch.tv/embed/sirbarbas/chat?darkpopout&parent=" +
        environment.domain
    );

    this.srcStream = this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://player.twitch.tv/?channel=sirbarbas&muted=true&parent=" +
        environment.domain
    );
  }
}
