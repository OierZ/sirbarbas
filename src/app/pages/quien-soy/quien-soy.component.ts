import { Component, OnInit } from "@angular/core"
import { Title } from "@angular/platform-browser"

@Component({
  selector: "app-quien-soy",
  templateUrl: "./quien-soy.component.html",
  styleUrls: ["./quien-soy.component.scss"]
})
export class QuienSoyComponent implements OnInit {
  constructor(private title: Title) {
    title.setTitle("YO - SirBarbas")
  }

  ngOnInit() {}
}
