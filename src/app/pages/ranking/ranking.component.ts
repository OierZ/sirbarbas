import { Component, OnInit } from "@angular/core";
import { RpgService } from "./../../services/rpg.service";

@Component({
  selector: "app-ranking",
  templateUrl: "./ranking.component.html",
  styleUrls: ["./ranking.component.scss"]
})
export class RankingComponent implements OnInit {
  rankingNivel;
  rankingPuntos;
  rankingPuntosMes;
  cargando = true;
  constructor(private rpg: RpgService) {
    this.rpg.getRankingNivel().subscribe(data => {
      this.cargando = false;
      this.rankingNivel = data;
    });

    this.rpg.getRankingPuntos().subscribe(data => {
      this.cargando = false;
      this.rankingPuntos = data;
    });

    this.rpg.getRankingPuntosMes().subscribe(data => {
      this.cargando = false;
      this.rankingPuntosMes = data;
    });
  }

  ngOnInit() {}
}
