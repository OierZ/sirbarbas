import { AuthService } from "./../../../services/auth.service";
import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  constructor(private title: Title, private auth: AuthService) {
    this.title.setTitle("SirBarbas - Login");
  }

  ngOnInit() {}

  loginTwitch() {
    this.auth.loginTwitch();
  }
}
