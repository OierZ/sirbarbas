import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Skin } from "src/app/model/skin";
import { User } from "src/app/model/user";
import { CanjearCodigoDialog } from "../../../components/dialog/canjear-codigo-dialog.component";
import { Equipamiento } from "./../../../model/equipamiento";
import { Inventario } from "./../../../model/inventario";
import { Objeto } from "./../../../model/objeto";
import { EquipamientoMapperService } from "./../../../services/equipamiento-mapper.service";
import { RpgService } from "./../../../services/rpg.service";

@Component({
  selector: "app-personaje",
  templateUrl: "./personaje.component.html",
  styleUrls: ["./personaje.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class PersonajeComponent implements OnInit {
  skinSeleccionada = -1;
  personajeInfo: User = new User();
  personajeInfoEquipado: User = Object.create(this.personajeInfo);
  equipoLocal = new Equipamiento();
  items: Objeto[];
  skins: Skin[];
  loreEditar: boolean = false;
  mostrarCarta: Objeto;
  isSingleClick: Boolean = true;
  quitarCartaClass: boolean = false;

  constructor(
    public dialog: MatDialog,
    public rpg: RpgService,
    private router: Router,
    private equipamientoMapper: EquipamientoMapperService
  ) {}

  onRightClick(ev) {
    ev.preventDefault();
  }

  ngOnInit() {
    this.getPersonajeInfo();
  }

  calcularEstadisticas() {
    this.personajeInfoEquipado = Object.create(this.personajeInfo);

    this.personajeInfoEquipado.damage +=
      this.equipoLocal.anillo1 != null
        ? this.equipoLocal.anillo1.damageBonus
        : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.anillo2 != null
        ? this.equipoLocal.anillo2.damageBonus
        : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.arma1 != null ? this.equipoLocal.arma1.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.arma2 != null ? this.equipoLocal.arma2.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.cabeza != null ? this.equipoLocal.cabeza.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.cuello != null ? this.equipoLocal.cuello.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.diploma != null
        ? this.equipoLocal.diploma.damageBonus
        : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.item1 != null ? this.equipoLocal.item1.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.item2 != null ? this.equipoLocal.item2.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.item3 != null ? this.equipoLocal.item3.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.manos != null ? this.equipoLocal.manos.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.pecho != null ? this.equipoLocal.pecho.damageBonus : 0;
    this.personajeInfoEquipado.damage +=
      this.equipoLocal.pies != null ? this.equipoLocal.pies.damageBonus : 0;

    this.personajeInfoEquipado.health +=
      this.equipoLocal.anillo1 != null
        ? this.equipoLocal.anillo1.healthBonus
        : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.anillo2 != null
        ? this.equipoLocal.anillo2.healthBonus
        : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.arma1 != null ? this.equipoLocal.arma1.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.arma2 != null ? this.equipoLocal.arma2.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.cabeza != null ? this.equipoLocal.cabeza.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.cuello != null ? this.equipoLocal.cuello.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.diploma != null
        ? this.equipoLocal.diploma.healthBonus
        : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.item1 != null ? this.equipoLocal.item1.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.item2 != null ? this.equipoLocal.item2.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.item3 != null ? this.equipoLocal.item3.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.manos != null ? this.equipoLocal.manos.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.pecho != null ? this.equipoLocal.pecho.healthBonus : 0;
    this.personajeInfoEquipado.health +=
      this.equipoLocal.pies != null ? this.equipoLocal.pies.healthBonus : 0;

    this.personajeInfoEquipado.armor +=
      this.equipoLocal.anillo1 != null
        ? this.equipoLocal.anillo1.armorBonus
        : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.anillo2 != null
        ? this.equipoLocal.anillo2.armorBonus
        : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.arma1 != null ? this.equipoLocal.arma1.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.arma2 != null ? this.equipoLocal.arma2.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.cabeza != null ? this.equipoLocal.cabeza.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.cuello != null ? this.equipoLocal.cuello.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.diploma != null
        ? this.equipoLocal.diploma.armorBonus
        : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.item1 != null ? this.equipoLocal.item1.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.item2 != null ? this.equipoLocal.item2.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.item3 != null ? this.equipoLocal.item3.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.manos != null ? this.equipoLocal.manos.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.pecho != null ? this.equipoLocal.pecho.armorBonus : 0;
    this.personajeInfoEquipado.armor +=
      this.equipoLocal.pies != null ? this.equipoLocal.pies.armorBonus : 0;

    this.personajeInfoEquipado.curation +=
      this.equipoLocal.anillo1 != null
        ? this.equipoLocal.anillo1.curationBonus
        : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.anillo2 != null
        ? this.equipoLocal.anillo2.curationBonus
        : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.arma1 != null ? this.equipoLocal.arma1.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.arma2 != null ? this.equipoLocal.arma2.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.cabeza != null
        ? this.equipoLocal.cabeza.curationBonus
        : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.cuello != null
        ? this.equipoLocal.cuello.curationBonus
        : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.diploma != null
        ? this.equipoLocal.diploma.curationBonus
        : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.item1 != null ? this.equipoLocal.item1.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.item2 != null ? this.equipoLocal.item2.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.item3 != null ? this.equipoLocal.item3.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.manos != null ? this.equipoLocal.manos.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.pecho != null ? this.equipoLocal.pecho.curationBonus : 0;
    this.personajeInfoEquipado.curation +=
      this.equipoLocal.pies != null ? this.equipoLocal.pies.curationBonus : 0;
  }

  /**
   * click derecho en un objeto del inventario
   *
   * @param item
   */
  clickInventario(item: Objeto) {
    this.isSingleClick = false;

    if (item.location == "head_slot") {
      if (this.equipoLocal.cabeza != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.cabeza);
      }
      this.equipoLocal.cabeza = item;
    }

    if (item.location == "neck_slot") {
      if (this.equipoLocal.cuello != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.cuello);
      }
      this.equipoLocal.cuello = item;
    }

    if (item.location == "torso_slot") {
      if (this.equipoLocal.pecho != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.pecho);
      }
      this.equipoLocal.pecho = item;
    }

    if (
      item.location.includes("ring_slot_1") &&
      !item.location.includes("ring_slot_2")
    ) {
      if (this.equipoLocal.anillo1 == null) {
        this.equipoLocal.anillo1 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.anillo1);
        this.equipoLocal.anillo1 = item;
      }
    } else if (
      item.location.includes("ring_slot_2") &&
      !item.location.includes("ring_slot_1")
    ) {
      if (this.equipoLocal.anillo2 == null) {
        this.equipoLocal.anillo2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.anillo2);
        this.equipoLocal.anillo2 = item;
      }
    } else if (
      item.location.includes("ring_slot_1") &&
      item.location.includes("ring_slot_2")
    ) {
      if (this.equipoLocal.anillo1 == null) {
        this.equipoLocal.anillo1 = item;
      } else if (this.equipoLocal.anillo2 == null) {
        this.equipoLocal.anillo2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.anillo1);
        this.equipoLocal.anillo1 = item;
      }
    }

    if (
      item.location.includes("weapon_slot_1") &&
      !item.location.includes("weapon_slot_2")
    ) {
      if (this.equipoLocal.arma1 == null) {
        this.equipoLocal.arma1 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.arma1);
        this.equipoLocal.arma1 = item;
      }
    } else if (
      item.location.includes("weapon_slot_2") &&
      !item.location.includes("weapon_slot_1")
    ) {
      if (this.equipoLocal.arma2 == null) {
        this.equipoLocal.arma2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.arma2);
        this.equipoLocal.arma2 = item;
      }
    } else if (
      item.location.includes("weapon_slot_1") &&
      item.location.includes("weapon_slot_2")
    ) {
      if (this.equipoLocal.arma1 == null) {
        this.equipoLocal.arma1 = item;
      } else if (this.equipoLocal.anillo2 == null) {
        this.equipoLocal.arma2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.arma1);
        this.equipoLocal.arma2 = item;
      }
    }

    if (
      item.location.includes("item_slot_1") &&
      !item.location.includes("item_slot_2") &&
      !item.location.includes("item_slot_3")
    ) {
      if (this.equipoLocal.item1 == null) {
        this.equipoLocal.item1 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item1);
        this.equipoLocal.item1 = item;
      }
    } else if (
      item.location.includes("item_slot_2") &&
      !item.location.includes("item_slot_1") &&
      !item.location.includes("item_slot_3")
    ) {
      if (this.equipoLocal.item2 == null) {
        this.equipoLocal.item2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item2);
        this.equipoLocal.item2 = item;
      }
    } else if (
      item.location.includes("item_slot_3") &&
      !item.location.includes("item_slot_2") &&
      !item.location.includes("item_slot_1")
    ) {
      if (this.equipoLocal.item3 == null) {
        this.equipoLocal.item3 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item3);
        this.equipoLocal.item3 = item;
      }
    } else if (
      item.location.includes("item_slot_1") &&
      item.location.includes("item_slot_2") &&
      !item.location.includes("item_slot_3")
    ) {
      if (this.equipoLocal.item1 == null) {
        this.equipoLocal.item1 = item;
      } else if (this.equipoLocal.item2 == null) {
        this.equipoLocal.item2 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item1);
        this.equipoLocal.item1 = item;
      }
    } else if (
      item.location.includes("item_slot_1") &&
      item.location.includes("item_slot_3") &&
      !item.location.includes("item_slot_2")
    ) {
      if (this.equipoLocal.item1 == null) {
        this.equipoLocal.item1 = item;
      } else if (this.equipoLocal.item3 == null) {
        this.equipoLocal.item3 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item1);
        this.equipoLocal.item1 = item;
      }
    } else if (
      item.location.includes("item_slot_2") &&
      item.location.includes("item_slot_3") &&
      !item.location.includes("item_slot_1")
    ) {
      if (this.equipoLocal.item2 == null) {
        this.equipoLocal.item2 = item;
      } else if (this.equipoLocal.item3 == null) {
        this.equipoLocal.item3 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item2);
        this.equipoLocal.item2 = item;
      }
    } else if (
      item.location.includes("item_slot_1") &&
      item.location.includes("item_slot_2") &&
      item.location.includes("item_slot_3")
    ) {
      if (this.equipoLocal.item1 == null) {
        this.equipoLocal.item1 = item;
      } else if (this.equipoLocal.item2 == null) {
        this.equipoLocal.item2 = item;
      } else if (this.equipoLocal.item3 == null) {
        this.equipoLocal.item3 = item;
      } else {
        this.equipoLocal.inventario.push(this.equipoLocal.item1);
        this.equipoLocal.item1 = item;
      }
    }

    if (item.location == "hand_slot") {
      if (this.equipoLocal.manos != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.manos);
      }
      this.equipoLocal.manos = item;
    }

    if (item.location == "feet_slot") {
      if (this.equipoLocal.pies != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.pies);
      }
      this.equipoLocal.pies = item;
    }

    if (item.location == "special_item_slot") {
      if (this.equipoLocal.diploma != null) {
        this.equipoLocal.inventario.push(this.equipoLocal.diploma);
      }
      this.equipoLocal.diploma = item;
    }

    const index = this.equipoLocal.inventario.indexOf(item);
    if (index > -1) {
      this.equipoLocal.inventario.splice(index, 1);
    }

    let invTMP: Inventario = this.equipamientoMapper.sendEquipamiento(
      this.equipoLocal
    );

    this.rpg.updateInventario(invTMP).subscribe(() => {
      this.calcularEstadisticas();
    });
    return false;
  }

  desLog() {
    localStorage.removeItem("access_token");
    this.router.navigate(["rpg"]);
  }

  clickEquipo(hueco) {
    this.isSingleClick = false;
    switch (hueco) {
      case "cabeza":
        this.equipoLocal.inventario.push(this.equipoLocal.cabeza);
        this.equipoLocal.cabeza = null;
        break;

      case "cuello":
        this.equipoLocal.inventario.push(this.equipoLocal.cuello);
        this.equipoLocal.cuello = null;
        break;

      case "anillo1":
        this.equipoLocal.inventario.push(this.equipoLocal.anillo1);
        this.equipoLocal.anillo1 = null;
        break;

      case "anillo2":
        this.equipoLocal.inventario.push(this.equipoLocal.anillo2);
        this.equipoLocal.anillo2 = null;
        break;

      case "pecho":
        this.equipoLocal.inventario.push(this.equipoLocal.pecho);
        this.equipoLocal.pecho = null;
        break;

      case "item1":
        this.equipoLocal.inventario.push(this.equipoLocal.item1);
        this.equipoLocal.item1 = null;
        break;

      case "item2":
        this.equipoLocal.inventario.push(this.equipoLocal.item2);
        this.equipoLocal.item2 = null;
        break;

      case "item3":
        this.equipoLocal.inventario.push(this.equipoLocal.item3);
        this.equipoLocal.item3 = null;
        break;
      case "manos":
        this.equipoLocal.inventario.push(this.equipoLocal.manos);
        this.equipoLocal.manos = null;
        break;
      case "pies":
        this.equipoLocal.inventario.push(this.equipoLocal.pies);
        this.equipoLocal.pies = null;
        break;
      case "diploma":
        this.equipoLocal.inventario.push(this.equipoLocal.diploma);
        this.equipoLocal.diploma = null;
        break;

      default:
        break;
    }

    let invTMP: Inventario = this.equipamientoMapper.sendEquipamiento(
      this.equipoLocal
    );

    this.rpg.updateInventario(invTMP).subscribe(() => {
      this.calcularEstadisticas();
    });
    return false;
  }

  getPersonajeInfo() {
    this.rpg.getUser().subscribe((data: User) => {
      if (Object.entries(data).length === 0 && data.constructor === Object) {
        this.rpg.setUser().subscribe((data: User) => {
          this.setEstadisticas(data);
          if (this.items) {
            this.getPersonajeInventario();
          } else {
            this.getItems();
          }
        });
      } else {
        this.setEstadisticas(data);
        if (this.items) {
          this.getPersonajeInventario();
        } else {
          this.getItems();
        }
      }
    });
  }

  getPersonajeInventario(): void {
    this.rpg.getInventario().subscribe((dataInv) => {
      if (
        Object.entries(dataInv).length === 0 &&
        dataInv.constructor === Object
      ) {
        this.rpg.setInventario().subscribe((data) => {
          this.equipoLocal = this.equipamientoMapper.getEquipamiento(
            data,
            this.items,
            this.skins
          );
          this.calcularEstadisticas();
          this.seleccionarSkin();
        });
      } else {
        this.equipoLocal = this.equipamientoMapper.getEquipamiento(
          dataInv,
          this.items,
          this.skins
        );
        this.calcularEstadisticas();
        this.seleccionarSkin();
      }
    });
  }

  getItems() {
    this.rpg.getItems().subscribe((data: Objeto[]) => {
      this.items = data;
      this.rpg.getSkins().subscribe((dataSkin: Skin[]) => {
        this.skins = dataSkin;

        this.getPersonajeInventario();
      });
    });
  }

  setEstadisticas(data: User) {
    this.personajeInfo = data;
    this.personajeInfoEquipado = Object.create(this.personajeInfo);
  }

  openDialog() {
    const dialogRef = this.dialog.open(CanjearCodigoDialog);
    dialogRef.afterClosed().subscribe((result) => {});
  }

  cambiarSkin(siguiente: boolean) {
    if (siguiente) {
      if (this.skinSeleccionada == this.equipoLocal.skins.length - 1)
        this.skinSeleccionada = 0;
      else this.skinSeleccionada++;
    } else {
      if (this.skinSeleccionada == 0)
        this.skinSeleccionada = this.equipoLocal.skins.length - 1;
      else this.skinSeleccionada--;
    }
  }

  equiparSkin() {
    if (
      this.equipoLocal.skins[this.skinSeleccionada].skinName !=
      this.equipoLocal.skin
    ) {
      this.rpg
        .setSkin(this.equipoLocal.skins[this.skinSeleccionada].skinName)
        .subscribe((data) => {
          this.equipoLocal = this.equipamientoMapper.getEquipamiento(
            data,
            this.items,
            this.skins
          );
          this.calcularEstadisticas();
          this.seleccionarSkin();
        });
    }
  }

  seleccionarSkin() {
    this.skinSeleccionada = this.equipoLocal.skins.findIndex(
      (element) => element.skinName == this.equipoLocal.skin
    );
  }

  guardarLore() {
    this.rpg.setLore(this.personajeInfo.lore).subscribe((data: User) => {
      this.personajeInfo.loreState = data.loreState;
      this.loreEditar = false;
    });
  }
  editarLore() {
    this.loreEditar = true;
  }

  clickMostrarCarta(item: Objeto) {
    this.quitarCartaClass = false;
    this.isSingleClick = true;
    setTimeout(() => {
      if (this.isSingleClick) {
        this.mostrarCarta = item;
      }
    }, 250);
  }
  quitarCarta() {
    this.quitarCartaClass = true;
    setTimeout(() => {
      this.mostrarCarta = null;
    }, 300);
  }
}
