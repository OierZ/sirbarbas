import { Component } from "@angular/core";
import { NavigationEnd, NavigationStart, Router } from "@angular/router";
import { AuthService } from "./services/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  constructor(private router: Router, private auth: AuthService) {
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationStart) {
        if (evt.url.includes("#access_token")) {
          let token = evt.url
            .substring(
              evt.url.indexOf("#access_token"),
              evt.url.indexOf("&scope")
            )
            .replace("#access_token=", "");

          this.auth.setAccessToken(token);
          this.auth.comprobarAuth(evt.url);
        } else if (evt.url.includes("/rpg")) {
          if (evt.id == 1) {
            router.navigate(["/rpg"]);
          } else {
            this.auth.comprobarAuth(evt.url);
          }
        }
      }

      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  mostrarHeader() {
    switch (this.router.url) {
      case "/":
      case "/backend":
        return false;
      default:
        return true;
    }
  }

  mostrarFooter() {
    switch (this.router.url) {
      case "/":
      case "/backend":
        return false;
      default:
        return true;
    }
  }

  mostrarStream() {
    switch (this.router.url) {
      case "/":
      case "/cookies":
      case "/twitch":
      case "/backend":
        return false;
      default:
        if (
          /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
          )
        ) {
          return false;
        }
        return true;
    }
  }

  mostrarCookies() {
    if (localStorage.getItem("cookiesHide")) {
      return false;
    }

    switch (this.router.url) {
      case "/":
      case "/backend":
        return false;
      default:
        return true;
    }
  }
}
